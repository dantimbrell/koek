# koek 

A simple cookiecutter template for personal projects.

You should be able to immediately apply it via: 

```bash
> pip install -U cookiecutter
> cookiecutter https://gitlab.com/dantimbrell/koek.git
```

Note that if you'd like to make changes that you also need to 
change the `cookiecutter.json` file in the root dir. 
